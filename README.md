# React App Template

React App Template is a template that include basic configuration for react application project, that utilize integration with TypeScript, Babel, Webpack, ESLint, Sass, MaterialUI and Redux.

## Installation

I used npm v.7.24.0 to install all packages.

```bash
npm i
```
