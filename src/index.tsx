import ReactDOM from "react-dom";
import { StrictMode } from "react";
import { App } from "./App";
import { store } from "./app/store";
import { Provider } from "react-redux";

ReactDOM.render(
  <StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </StrictMode>,
  document.getElementById("root")
);
