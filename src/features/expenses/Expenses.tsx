import React from "react";
//Redux
// import { useAppSelector, useAppDispatch } from "../../app/hook";
// import { getAllRecords, addRecord } from "./expensesSlice";
// Custom CSS
import expensesStyles from "../expenses/expenses.scss";
// MaterialUI
import { Box, TextField, Grid, Button, Container } from "@mui/material/";
import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";
// Other
import { format } from "date-fns";

const columns: GridColDef[] = [
  { field: "id", headerName: "ID", width: 70 },
  { field: "Date", headerName: "Date", width: 130 },
  { field: "lastName", headerName: "Last name", width: 130 },
  {
    field: "age",
    headerName: "Age",
    type: "number",
    width: 90,
  },
  {
    field: "fullName",
    headerName: "Full name",
    description: "This column has a value getter and is not sortable.",
    sortable: false,
    width: 160,
    valueGetter: (params: GridValueGetterParams) =>
      `${params.getValue(params.id, "firstName") || ""} ${
        params.getValue(params.id, "lastName") || ""
      }`,
  },
];

const rows = [
  { id: 1, Date: "dSnow", firstName: "Jon", age: 35 },
  { id: 2, Date: "dLannister", firstName: "Cersei", age: 42 },
  { id: 3, Date: "dLannister", firstName: "Jaime", age: 45 },
  { id: 4, Date: "dStark", firstName: "Arya", age: 16 },
  { id: 5, Date: "dTargaryen", firstName: "Daenerys", age: null },
  { id: 6, Date: "dMelisandre", firstName: null, age: 150 },
  { id: 7, Date: "dClifford", firstName: "Ferrara", age: 44 },
  { id: 8, Date: "dFrances", firstName: "Rossini", age: 36 },
  { id: 9, Date: "dRoxie", firstName: "Harvey", age: 65 },
];

export function Expenses() {
  // The `state` arg is correctly typed as `RootState` already in /app/hook.tsx
  //   const expenses = useAppSelector((state) =>
  //     state.expenses.map((ele) => JSON.stringify(ele))
  //   );
  //   const dispatch = useAppDispatch();

  // omit rendering logic
  //   const [value, setValue] = React.useState<Date | null>(null);

  React.useEffect(() => {
    console.log("Did mount!");
  }, []);

  return (
    <React.Fragment>
      <Box className={expensesStyles.main}>
        <Box className={expensesStyles.board}>
          <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} md={3}>
                <Box className={expensesStyles.leftSection}>
                  <Container>LEFT SECTION</Container>
                </Box>
              </Grid>
              <Grid item xs={12} md={9}>
                <Box
                  component="form"
                  sx={{
                    "& .MuiTextField-root": {
                      m: 1,
                      width: "100%",
                    },
                  }}
                  noValidate
                  autoComplete="off"
                  className={expensesStyles.rightSection}
                >
                  <Container
                    style={{ display: "flex", justifyContent: "center" }}
                  >
                    <TextField
                      disabled
                      id="id-text-field"
                      label="ID"
                      defaultValue="1000"
                    />

                    <TextField
                      disabled
                      id="outlined-disabled"
                      label="Date"
                      defaultValue={format(new Date(), "dd.MM.yyyy")}
                    />
                  </Container>
                  <Container
                    style={{ display: "flex", justifyContent: "center" }}
                  >
                    <TextField
                      id="outlined-text-input"
                      label="Value"
                      type="text"
                      style={{ width: "30%" }}
                    />
                    <TextField
                      id="outlined-text-input"
                      label="Description"
                      type="text"
                    />
                  </Container>
                </Box>
                <Container>
                  <Button
                    variant="contained"
                    className={expensesStyles.saveRecordButton}
                  >
                    Save
                  </Button>
                </Container>
                <Box style={{ margin: "5px" }}>
                  <Box style={{ height: 400, width: "100%" }}>
                    <Box style={{ display: "flex", height: "100%" }}>
                      <Box style={{ flexGrow: 1 }}>
                        <DataGrid
                          rows={rows}
                          columns={columns}
                          pageSize={5}
                          rowsPerPageOptions={[5]}
                        />
                      </Box>
                    </Box>
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Box>
    </React.Fragment>
  );
}
