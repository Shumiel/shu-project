import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../../app/store";

// Define a type for the slice state
interface expensesState {
  id: number;
  category: string;
  value: number;
  description: string;
  date: string;
}

type arrayOfObjects = expensesState[];

// Define the initial state using that type
const initialState: arrayOfObjects = [
  {
    id: 0,
    category: "test",
    value: 0,
    description: "",
    date: "",
  },
  {
    id: 1,
    category: "test",
    value: 0,
    description: "",
    date: "",
  },
  {
    id: 2,
    category: "test",
    value: 0,
    description: "",
    date: "",
  },
];

export const expensesSlice = createSlice({
  name: "expenses",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    getAllRecords: (state) => {
      state.map((ele) => console.log(JSON.stringify(ele)));
    },
    addRecord: (state, action: PayloadAction<expensesState>) => {
      state.push(action.payload);
      console.log(JSON.stringify(state));
    },
  },
});

export const { getAllRecords, addRecord } = expensesSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.expenses;

export default expensesSlice.reducer;
