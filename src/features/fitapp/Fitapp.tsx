import React, { useEffect } from "react";

//Custome Components
import ThreeBackground from "./components/ThreeBackground";

const Fitapp: React.FC = () => {
  useEffect(() => {}, []);

  return (
    <React.Fragment>
      <ThreeBackground />
    </React.Fragment>
  );
};

export default Fitapp;
