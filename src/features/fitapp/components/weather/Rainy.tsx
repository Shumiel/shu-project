import * as THREE from "three";
import React, { useRef, useEffect } from "react";
import smoke from "../weather/smoke.png";

const Rainy: React.FC = () => {
  const mountRef = useRef<any>(null);

  useEffect(() => {
    //Scene & camera section
    let scene = new THREE.Scene();
    let camera = new THREE.PerspectiveCamera(
      60,
      window.innerWidth / window.innerHeight,
      1,
      1000
    );
    camera.position.z = 1;
    camera.rotation.x = 1.16;
    camera.rotation.y = -0.12;
    camera.rotation.z = 0.27;

    //Ambient, light and flash section
    let ambient = new THREE.AmbientLight(0x555555);
    scene.add(ambient);
    let directionalLight = new THREE.DirectionalLight(0xffeedd);
    directionalLight.position.set(0, 0, 1);
    scene.add(directionalLight);
    let flash = new THREE.PointLight(0x062d89, 30, 500, 1.7);
    flash.position.set(200, 300, 100);
    scene.add(flash);

    //Renderer section
    let renderer = new THREE.WebGLRenderer();
    scene.fog = new THREE.FogExp2(0x11111f, 0.002);
    renderer.setClearColor(scene.fog.color);
    renderer.setSize(window.innerWidth, window.innerHeight);
    mountRef.current.appendChild(renderer.domElement);
    let pts = [];
    let velocity = [];
    let rainCount = 12000;
    for (let i = 0; i < rainCount; i++) {
      let y = Math.random() * 500 - 250;
      pts.push(
        new THREE.Vector3(
          Math.random() * 400 - 200,
          y,
          Math.random() * 400 - 200
        )
      );
      velocity.push({ y: y, velocity: 0 });
    }
    //
    let rainGeo = new THREE.BufferGeometry().setFromPoints(pts);
    let rainMaterial = new THREE.PointsMaterial({
      color: 0xaaaaaa,
      size: 0.1,
      transparent: true,
    });
    let rain = new THREE.Points(rainGeo, rainMaterial);
    rain.userData = {
      velocity: velocity,
    };
    scene.add(rain);
    let cloudParticles: THREE.Mesh<
      THREE.PlaneGeometry,
      THREE.MeshLambertMaterial
    >[] = [];
    let loader = new THREE.TextureLoader();
    loader.load(
      smoke,
      (texture) => {
        console.log(texture);
        let cloudGeo = new THREE.PlaneBufferGeometry(500, 500);
        let cloudMaterial = new THREE.MeshLambertMaterial({
          map: texture,
          transparent: true,
        });
        for (let p = 0; p < 25; p++) {
          let cloud = new THREE.Mesh(cloudGeo, cloudMaterial);
          cloud.position.set(
            Math.random() * 800 - 400,
            500,
            Math.random() * 500 - 450
          );
          cloud.rotation.x = 1.16;
          cloud.rotation.y = -0.12;
          cloud.rotation.z = Math.random() * 360;
          cloud.material.opacity = 0.6;
          cloudParticles.push(cloud);
          scene.add(cloud);
        }
        animate();
      },
      (error) => {
        console.log(error);
      }
    );

    let onWindowResize = () => {
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
      renderer.setSize(window.innerWidth, window.innerHeight);
    };
    window.addEventListener("resize", onWindowResize, false);

    let animate = () => {
      cloudParticles.forEach((p) => {
        p.rotation.z -= 0.002;
      });
      rain.userData.velocity.forEach(
        (p: { velocity: number; y: number }, idx: number) => {
          p.velocity -= 0.1 + Math.random() * 0.1;
          p.y += p.velocity;
          if (p.y < -200) {
            p.y = 200;
            p.velocity = 0;
          }
          rainGeo.attributes.position.setY(idx, p.y);
          rainGeo.attributes.position.needsUpdate = true;
        }
      );
      rain.rotation.y += 0.002;
      if (Math.random() > 0.93 || flash.power > 100) {
        if (flash.power < 100)
          flash.position.set(
            Math.random() * 400,
            300 + Math.random() * 200,
            100
          );
        flash.power = 50 + Math.random() * 500;
      }
      renderer.render(scene, camera);
      requestAnimationFrame(animate);
    };
    animate();
    let mountRefCurrent = mountRef.current; //eslint typo fix
    return () => mountRefCurrent.removeChild(renderer.domElement);
  }, []);

  return <div ref={mountRef}></div>;
};

export default Rainy;
