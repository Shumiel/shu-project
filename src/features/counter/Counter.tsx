import React from "react";
//Redux - START
import { useAppSelector, useAppDispatch } from "../../app/hook";
import { decrement, increment } from "./counterSlice";
//REDUX - END

export function Counter() {
  // The `state` arg is correctly typed as `RootState` already
  const count = useAppSelector((state) => state.counter.value);
  const dispatch = useAppDispatch();

  // omit rendering logic

  return (
    <div style={{ zIndex: 100 }}>
      <input
        type="button"
        value={"+"}
        onClick={() => {
          dispatch(increment());
          console.log("Redux - Increment", count);
        }}
      />

      <input type="button" value={count} />

      <input
        type="button"
        value={"-"}
        onClick={() => {
          dispatch(decrement());
          console.log("Redux - Decrement", count);
        }}
      />
    </div>
  );
}
