import React from "react";
// import IMAGE from "./img/react.png";
import "./App.css";
// import scssStyles from "./styles.scss";
// import Button from "@mui/material/Button";

import Fitapp from "./features/fitapp/Fitapp";

export const App = () => {
  return (
    <React.Fragment>
      <div className="App">
        <Fitapp />
      </div>
    </React.Fragment>
  );
};
